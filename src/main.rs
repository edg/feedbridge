use url::Url;
use serde_derive::Deserialize;
use http::header::CONTENT_TYPE;
use atom_syndication::{Feed, Entry, Link, Content};
use chrono::{TimeZone, Utc};


static APP_USER_AGENT: &str = concat!(
    env!("CARGO_PKG_NAME"),
    "/",
    env!("CARGO_PKG_VERSION"),
);

#[derive(Debug, Deserialize)]
struct SubredditSubmissionData {
    pub name: String,
    pub author_fullname: String,
    pub title: String,
    pub url: String,
    pub created_utc: f64,
}

#[derive(Debug, Deserialize)]
struct SubredditSubmission {
    pub kind: String,
    pub data: SubredditSubmissionData,
}

#[derive(Debug, Deserialize)]
struct SubredditListingData {
    pub modhash: String,
    pub dist: u32,
    pub after: Option<String>,
    pub before: Option<String>,
    pub children: Vec<SubredditSubmission>,
}

#[derive(Debug, Deserialize)]
struct SubredditListing {
    pub kind: String,
    pub data: SubredditListingData,
}


async fn truereddit(client: &reqwest::Client) -> String {
    let resp = client.get("https://www.reddit.com/r/TrueReddit/.json?limit=25").send().await.unwrap().json::<SubredditListing>().await.unwrap();
    let mut feed = Feed::default();
    let mut feed_at = Utc.timestamp(0, 0);
    feed.set_title("TrueReddit");
    feed.set_id("https://www.reddit.com/r/TrueReddit");
    feed.set_icon("https://www.redditstatic.com/icon.png/".to_string());
    let mut entries = vec![];
    for child in resp.data.children {
        let entry_at = Utc.timestamp(child.data.created_utc as i64, 0);
        let mut entry = Entry::default();
        let mut link = Link::default();
        link.set_href(&child.data.url);
        entry.set_id(&child.data.name);
        entry.set_title(&child.data.title);
        entry.set_updated(entry_at);
        entry.set_links(vec![link]);
        let url = Url::parse(&child.data.url).unwrap();
        let article_resp = client.head(&child.data.url).send().await.unwrap();
        if article_resp.headers().contains_key(CONTENT_TYPE) && article_resp.headers()[CONTENT_TYPE].to_str().unwrap().starts_with("text/html") {
            let article_resp = client.get(&child.data.url).send().await.unwrap();
            let text = article_resp.text().await.unwrap();
            if let Ok(article) = readability::extractor::extract(&mut text.as_bytes(), &url) {
                let article_escaped = htmlescape::encode_minimal(&article.content);
                let mut content = Content::default();
                content.set_content_type("html".to_string());
                content.set_value(article_escaped);
                entry.set_content(content);
            }
        }
        entries.push(entry);
        if feed_at < entry_at {
            feed_at = entry_at;
        }
    }
    feed.set_updated(feed_at);
    feed.set_entries(entries);
    return feed.to_string();
}

#[tokio::main]
async fn main() {
    let client = reqwest::Client::builder()
        .user_agent(APP_USER_AGENT)
        .build().unwrap();
    let rss = truereddit(&client).await;
    println!("{}", rss);
}
